from MyMQTT import *
import time
import requests

class CatalogPublisher():
    def __init__(self, topic, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port)
        self.topic = topic

    def start(self):
        self.client.start()

    def stop(self):
        self.client.stop()

    def publish(self, message):
        self.client.myPublish(self.topic, message)
        print('Published.')


if __name__ == '__main__':
    conf = json.load(open('settings.json'))
    broker = conf['broker']
    port = conf['port']
    topic = conf['baseTopic'] + '/catalog'

    r = requests.get('http://localhost:4040/api/tunnels/catalog')
    catalog_REST = r.json()["public_url"]

    fridge_scanner_manager = CatalogPublisher(topic, 'CatalogPublisher', broker, port)
    fridge_scanner_manager.start()
    fridge_scanner_manager.publish(catalog_REST)
    fridge_scanner_manager.stop()
