### ENTRY INFORMATION: message: product to search; start_grid: initial search parameter. ###
import cherrypy
import requests
from lxml import html
import json

class ProductsCatalog(object):
    exposed=True
    def __init__(self):
        pass

    def GET(self, *uri, **params):
        subcatalog = uri[0]
        open_catalog = json.load(open(f'{subcatalog}.json', 'r'))
        open_catalog_str = json.dumps(open_catalog)
        return open_catalog_str

    def PUT(self, *uri, **params):
        subcatalog = uri[0]

        body = cherrypy.request.body.read()
        subcatalog_info = json.loads(body)

        if subcatalog=='home_catalog' or subcatalog=='shopping_list':
            updated_subcatalog = {subcatalog: subcatalog_info}
        elif subcatalog=='user_settings':
            updated_subcatalog = {'households': subcatalog_info}
        else:
            updated_subcatalog = subcatalog_info

        with open(f'{subcatalog}.json', 'w') as json_subcatalog:
            # Write file:
            json.dump(updated_subcatalog, json_subcatalog)

        print(f'{subcatalog} updated.')



if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8010})
    cherrypy.quickstart(ProductsCatalog(),'/',conf)
