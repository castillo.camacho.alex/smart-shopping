from MyMQTT import *
import time
import json
import requests

catalog_REST = json.load(open("settings.json"))['catalog_REST']

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

def update_catalog(subcatalog, body):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.put(url, data=json.dumps(body))


class AddItemShoppingListSubscriber():
    def __init__(self, topic, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)
        self.topic = topic

    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)

    def stop(self):
        self.client.stop()

    def notify(self, topic, payload):
        msg = json.loads(payload)
        msg = msg.split(' ')
        barcode = msg[0]
        username = msg[1]
        household_ID = msg[2]

        home_catalog = catalog_search('home_catalog', 'home_catalog')

        # Finds the product with that name:
        for hh in home_catalog:
            if hh['household']==household_ID:
                for prod in hh['products']:
                    if prod['barcode']==barcode:
                        # Adjusts the quantity of remaining items:
                        prod['quantity'] -= 1

                        # Remove item from Home catalog
                        home_catalog_json = {'home_catalog': home_catalog}
                        url = f'{products_catalog_REST}/home_catalog'
                        n = requests.put(url, data=json.dumps(home_catalog_json))

                        print(f'1 unit of {product_name} removed from the Home catalog by {username} (HOUSEHOLD: {household_ID}).')

                        # Get user_settings (n_items_before_shopping)
                        user_settings = catalog_search('user_settings', 'households')

                        for hh in user_settings:
                            if hh['household_ID']==household_ID:
                                n_items_before_shopping = hh['general_settings']['n_items_before_shopping']

                        # Get product information
                        product_name = prod['product_name']
                        quantity = prod['quantity']
                        quantity = prod['original_quantity']
                        price = prod['price']
                        last_quantity_bought = list(prod['quantity_log'].values())[-1]

                        # Add product to Shopping list
                        if (last_quantity_bought>1 and quantity==n_items_before_shopping) or (last_quantity_bought==1 and quantity==0):
                            # Item to add to Shopping list
                            item = {
                            "barcode": barcode,
                            "product_name": product_name,
                            "quantity": quantity,
                            "price": price}

                            # Open Shopping list
                            shopping_list = catalog_search('shopping_list', 'shopping_list')

                            hh_not_in_shopping_list = True
                            for hh in shopping_list:
                                if hh['household']==household_ID:
                                    hh_not_in_shopping_list = False
                                    hh_shopping_list = hh['products']

                            if hh_not_in_shopping_list:
                                shopping_list.append({"household": household_ID, "products":[]})
                                hh_shopping_list = []

                            # Modify contents
                            for prod in shopping_list['products']:
                                if prod['household']==household_ID:
                                    hh_shopping_list.remove(prod)
                            hh_shopping_list.append(item)

                            for hh in shopping_list:
                                if hh['household']==household_ID:
                                    hh['products'] = hh_shopping_list
                            shopping_list_json = {'shopping_list': shopping_list}

                            # Update Shopping list
                            url = f'{products_catalog_REST}/shopping_list'
                            n = requests.put(url, data=json.dumps(shopping_list_json))

                            print(f'{quantity} unit(s) of {product_name} added to the Shopping list.')


if __name__ == '__main__':
    conf = catalog_search('settings')
    broker = conf['broker']
    port = conf['port']
    topic = conf['baseTopic'] + '/add-item-shopping-list3'

    my_shopping_list = AddItemShoppingListSubscriber(topic, 'ShoppingListSubscriber', broker, port)

    my_shopping_list.start()

    while True:
        time.sleep(3)

    my_shopping_list.stop()
