from MyMQTT import *
import time
import datetime
import json
import requests
from lxml import html
import statistics as st

## Create Class for publisher and subscriber of expiry_date to Telegram bot.

catalog_REST = json.load(open("settings.json"))['catalog_REST']

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

def update_catalog(subcatalog, body):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.put(url, data=json.dumps(body))


class AddItemHomeCatalogSubscriber():
    def __init__(self, topic, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)
        self.topic = topic

    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)

    def stop(self):
        self.client.stop()

    def notify(self, topic, payload):
        msg = json.loads(payload)
        msg = msg.split(' ')
        barcode = msg[0]
        expiry_date = [int(msg[3]),int(msg[2]),int(msg[1])]
        username = msg[4]
        household_ID = msg[5]
        quantity = int(msg[6])

        timestamp = datetime.datetime.now().timestamp()

        home_catalog = catalog_search('home_catalog', 'home_catalog')

        # Code that opens a question in the Telegram bot to add the expiry_date (publisher).
        ## We call right here the publisher (start, publish, stop).
        ## We also call here the subscriber that receives the answer from the Telegram bot.

        hh_not_in_home_catalog = True
        for hh in home_catalog:
            if hh['household']==household_ID:
                hh_not_in_home_catalog = False
        if hh_not_in_home_catalog:
            home_catalog.append({"household": household_ID, "products":[]})

        for hh in home_catalog:
            if hh['household']==household_ID:
                for prod in hh['products']:
                    prod_not_in_home_catalog = True
                    if prod['barcode']==barcode:
                        # Finds the product with that name:
                        product_name = prod['product_name']
                        price = prod['price']

                        # Calculates the original_quantity:
                        quantity_log_list = list(adding_product['quantity_log'].values())
                        original_quantity = round(st.mean(st.multimode(quantity_log_list)))

                        # Adjusts the quantity of remaining items:
                        prod['quantity'] += quantity
                        prod['original_quantity'] = original_quantity
                        prod['expiry_date'] = expiry_date
                        prod['cumulative_quantity'] += quantity
                        prod['quantity_log'][timestamp] = quantity

                        update_catalog('home_catalog', home_catalog)
                        # Code that updates the original_quantity.

                if hh_not_in_home_catalog:
                    url = 'https://www.carrefour.it/search?q=' + barcode
                    n = requests.get(url)
                    tree = html.fromstring(n.content)
                    product_name = tree.xpath('//h3[@class="tile-description"]/text()')[0] + ' - ' + tree.xpath('//h2[@class="brand"]/text()')[0]
                    try:
                        price = float(tree.xpath('//span[@class="value "]/text()')[0]
                                .strip('\n€ ')
                                .replace(',','.'))
                    except:
                        price = float(tree.xpath('//span[@class="value discounted"]/text()')[0]
                                .strip('\n€ ')
                                .replace(',','.'))

                    # To add item to the Home catalog:
                    new_item = catalog_search('item_template')

                    new_item['product_name'] = product_name
                    new_item['quantity'] = quantity
                    new_item['price'] = price
                    new_item['original_quantity'] = quantity
                    new_item['expiry_date'] = expiry_date
                    new_item['cumulative_quantity'] = quantity
                    new_item['quantity_log'][timestamp] = quantity

                    hh['products'].append(new_item)

        update_catalog('home_catalog', home_catalog)

        print(f'1 unit of {product_name} added to the Home catalog by {username} (HOUSEHOLD: {household_ID}).')

        # To send item to Thingspeak:
        user_settings = catalog_search('user_settings', 'households')

        for hh in user_settings:
            if hh['household']==household_ID:
                thingspeak_write_API = hh['general_settings']['thingspeak_write_API']
        r = requests.get(f'https://api.thingspeak.com/update?api_key={thingspeak_write_API}&field1={product_name}&field2={price}&field3={quantity}')


if __name__=="__main__":
    conf = catalog_search('settings')
    broker = conf['broker']
    port = conf['port']
    topic = conf['baseTopic'] + '/add-item-home-catalog3'

    my_home_catalog = AddItemHomeCatalogSubscriber(topic, 'HomeCatalogSubscriber', broker, port)

    my_home_catalog.start()

    while True:
        time.sleep(3)

    my_home_catalog.stop()
