# # '''
# # This code takes values calulated and puts them into a variable called f which consists of an baseURL which is specific for your project
# # since it is defined by the api_key. The variable f is the link including the new value that inserts a new measurment point to your
# # thingspeak channel. the f.read() command opens this link and the f.close command closes it. The variable that is transmitted can be
# # changed as you want to. In this case it calculates Fibonnaci Numbers.
# # '''
# #
# #
# # # from time import sleep
# # # import urllib.request
# # #
# # # a = 0
# # # b = 1
# # # c = 0
# # # baseURL = 'http://api.thingspeak.com/update?api_key=HTIGXMPIUQUJGN76'
# # #
# # # f = urllib.request.urlopen(baseURL + '&field1=cheese&field2=14.50&field3=3')
# # # f.read()
# # # f.close()
# # # sleep(20)
# # #
# # # f = urllib.request.urlopen(baseURL + '&field1=milk&field2=2.50&field3=6')
# # # f.read()
# # # f.close()
# # #
# # # print('Program has ended')
# #
# # # First read the data of the Channel (https://it.mathworks.com/help/thingspeak/readdata.html):
# # import requests
# # r = requests.get(f'https://api.thingspeak.com/channels/1425656/feeds.json?api_key=CSJPGH6BJLWIJCJG')
# # # LIMITATION: we take all the entries, with a maximum of 8000, which means that going shopping every day the channel would reach its limit in about 22 years.
# # channel_data = r.json()
# # feeds = channel_data['feeds']
# #
# # import time
# # from datetime import datetime, timedelta
# # # Process data:
# # pr_data = {}
# # for entry in feeds:
# #     year_month = entry['created_at'][0:7]
# #     if year_month in pr_data:
# #         pr_data[year_month][1] += float(entry['field2'])*float(entry['field3'])
# #     else:
# #         pr_data[year_month] = [datetime.strptime(year_month, '%Y-%m')]
# #         pr_data[year_month].append(float(entry['field2'])*float(entry['field3']))
# #
# # months_vector = []
# # expenses_vector = []
# # if len(pr_data)<12:
# #     #Empty expenses:
# #     expenses_vector = [0]*(12-len(pr_data))
# #     # Empty months:
# #     i = 12-len(pr_data)
# #     year = int(min(list(pr_data.keys()))[0:4])
# #     month = int(min(list(pr_data.keys()))[5:7])
# #     while i>0:
# #         new_month = month-i
# #         if new_month<=0:
# #             new_month += 12
# #             new_year = year - 1
# #             new_date = datetime.strptime(f'{new_year}-{new_month}', '%Y-%m')
# #         else:
# #             new_date = datetime.strptime(f'{year}-{new_month}', '%Y-%m')
# #         months_vector.append(new_date)
# #         i-=1
# # months_vector += [pr_data[mth][0] for mth in pr_data]
# # expenses_vector += [pr_data[mth][1] for mth in pr_data]
# #
# # # Send data to Thingspeak:
# # import json
# # with open('settings.json', 'r') as settings_json:
# #     settings = json.load(settings_json)
# #
# # processing_write_api_key = settings['processing_write_api_key']
# # processing_read_api_key = settings['processing_read_api_key']
# # processing_channel_id = settings['processing_channel_id']
# #
# # # bulk_update = {"write_api_key": processing_write_api_key, "updates": []}
# # # for i in range(len(expenses_vector)):
# # #     entry = {'created_at':str(months_vector[i]), 'field1':expenses_vector[i]}
# # #     bulk_update['updates'].append(entry)
# # # bulk_update_json = json.dumps(bulk_update)
# # # print(bulk_update)
# # #
# # # requests.post(f'https://api.thingspeak.com/channels/{processing_channel_id}/bulk_update.json', data=bulk_update_json)
# #
# # # for i in range(len(expenses_vector)):
# # #     r = requests.post(f'https://api.thingspeak.com/update?api_key={processing_write_api_key}&field1={expenses_vector[i]}&created_at={str(months_vector[i])}')
# # #     print(f'month {i}')
# # #     time.sleep(20)
# #
# # # print('postpost')
# #
# #
# # # Plotting:
# # print(f'https://api.thingspeak.com/channels/{processing_channel_id}/charts/1?api_key={processing_read_api_key}&type=column&title=Monthly expenses in a year&xaxis=Months&yaxis=Expenses')
# # ## Plotting in Python:
# # from matplotlib import pyplot as plt
# # plt.bar(months_vector, expenses_vector, width=20)
# # plt.title(f'Monthly expenses in a year')
# # plt.xlabel('Months')
# # plt.ylabel('Expenses')
# # print(type(plt))
#
# import requests
# from lxml import html
# from statistics import multimode, mean
#
# msg = '3046920028721 2021 09 13 trial Àlex 3'
# msg = msg.split(' ')
# barcode = msg[0]
# expiry_date = [msg[3],msg[2],msg[1]]
# household_ID = msg[4]
# username = msg[5]
# quantity = msg[6]
#
# url = 'https://www.carrefour.it/search?q=' + barcode
# n = requests.get(url)
# tree = html.fromstring(n.content)
# name = tree.xpath('//h3[@class="tile-description"]/text()')[0]
# print(name)
# brand = tree.xpath('//h2[@class="brand"]/text()')[0]
# print(brand)
# product_name = name + ' - ' + brand
# print(product_name)
# try:
#     price = float(tree.xpath('//span[@class="value "]/text()')[0]
#                         .strip('\n€ ')
#                         .replace(',','.'))
# except:
#     print('no price')
#     price = float(tree.xpath('//span[@class="value discounted"]/text()')[0]
#                         .strip('\n€ ')
#                         .replace(',','.'))
# print(price)
#
# from datetime import date
#
# a = date(date.today().year, date.today().month, 1)
# print(a)

import requests

url = 'https://scraping-3s.loca.lt'
url1 = 'https://wet-cougar-42.loca.lt'
url2 = 'http://127.0.0.1:8010'
n = requests.get(f'{url1}/settings')
print(n)
settings = n.json()
print(settings['all_chat_IDs'])
