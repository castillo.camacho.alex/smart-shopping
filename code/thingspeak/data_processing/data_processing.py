### ENTRY INFORMATION: message: product to search; start_grid: initial search parameter. ###
import cherrypy
import requests
from lxml import html
import json
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date


catalog_REST = json.load(open("settings.json"))['catalog_REST']

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

def date_format(pr_data):
    data = {}
    for entry in pr_data:
        dt = entry.split('-')
        date_ymd = date(int(dt[0]), int(dt[1]), int(dt[2]))
        data[date_ymd] = pr_data[entry]
    return data

class DataProcessing(object):
    exposed=True
    def __init__(self):
        pass

    def GET(self, *uri, **params):
        type_graph = uri[0]
        thingspeak_channel_id = params['thingspeak_channel_id']
        thingspeak_read_API = params['thingspeak_read_API']

        base_url = catalog_search('settings', 'thingspeak_adaptor_REST')

        if type_graph=='1': # MONTHLY EXPENSES IN A YEAR: we consider a year 12 months.
            url = f'{base_url}/months?thingspeak_channel_id={thingspeak_channel_id}&thingspeak_read_API={thingspeak_read_API}'
            n = requests.get(url)
            pr_data = n.json()
            data = date_format(pr_data)

            months_vector = []
            expenses_vector = [0]*12
            month_year = date(date.today().year, date.today().month, 1)
            delta = relativedelta(months=1)
            for i in range(12):
                months_vector = [month_year.strftime("%m/%y")]+months_vector
                if month_year in data:
                    expenses_vector[-i-1] = data[month_year]
                month_year -= delta
            title = f'Monthly expenses in a year'
            x_axis = 'Days'

            time_period, expenses = months_vector, expenses_vector

        if type_graph=='2': # DAILY EXPENSES IN A MONTH: we consider a month 28 days (4 weeks).
            url = f'{base_url}/days?thingspeak_channel_id={thingspeak_channel_id}&thingspeak_read_API={thingspeak_read_API}'
            n = requests.get(url)
            pr_data = n.json()
            data = date_format(pr_data)

            days_vector = []
            expenses_vector = [0]*28
            date_ymd = date.today()
            delta = timedelta(days=1)
            for i in range(28):
                days_vector = [date_ymd.strftime("%d/%m")]+days_vector
                if date_ymd in data:
                    expenses_vector[-i-1] = data[date_ymd]
                date_ymd -= delta
            title = f'Daily expenses in a month'
            x_axis = 'Days'
            time_period, expenses = days_vector, expenses_vector

        if type_graph=='3': # DAILY EXPENSES IN A WEEK: we consider a week 7 days.
            url = f'{base_url}/days?thingspeak_channel_id={thingspeak_channel_id}&thingspeak_read_API={thingspeak_read_API}'
            n = requests.get(url)
            pr_data = n.json()
            data = date_format(pr_data)

            days_vector = []
            expenses_vector = [0]*7
            date_ymd = date.today()
            delta = timedelta(days=1)
            for i in range(7):
                days_vector = [date_ymd.strftime("%d/%m")]+days_vector
                if date_ymd in data:
                    expenses_vector[-i-1] = data[date_ymd]
                date_ymd -= delta
            title = f'Daily expenses in a week'
            x_axis = 'Days'
            time_period, expenses = days_vector, expenses_vector

        fig = plt.figure()
        print('Plotting...')
        plt.bar(time_period, expenses, width=0.5)
        print('Plot done. Saving...')
        plt.title(title)
        plt.xlabel(x_axis)
        plt.xticks(rotation=45, ha='right')
        plt.ylabel('Expenses')

        return fig


if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8083"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8032})
    cherrypy.quickstart(DataProcessing(),'/',conf)
