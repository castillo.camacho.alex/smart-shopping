### ENTRY INFORMATION: message: product to search; start_grid: initial search parameter. ###
import cherrypy
import requests
from lxml import html
import json
from datetime import datetime, timedelta, date

catalog_REST = json.load(open("settings.json"))['catalog_REST']

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

class ThingspeakAdaptor(object):
    exposed=True
    def __init__(self):
        pass

    def GET(self, *uri, **params):
        division = uri[0]
        thingspeak_channel_id = params['thingspeak_channel_id']
        thingspeak_read_API = params['thingspeak_read_API']

        base_url = catalog_search('settings', 'thingspeak_data_REST')
        thingspeak_data_url = f'{base_url}/feeds?thingspeak_channel_id={thingspeak_channel_id}&thingspeak_read_API={thingspeak_read_API}'
        n = requests.get(thingspeak_data_url)
        feeds = n.json()

        ### PREPARE THE TIME SPAN YOU WANT
        print(f'\nfeeds: {feeds}')
        if division=='days':
            pr_data = {} # Empty dictionary with all the expenses of each day.
            for entry in feeds: # The feeds is the data from Thingspeak.
                year = int(entry['created_at'][0:4])
                month = int(entry['created_at'][5:7])
                day = int(entry['created_at'][8:10])
                date_ymd = f'{year}-{month}-{day}'
                if date_ymd in pr_data:
                    pr_data[date_ymd] += float(entry['field2'])*float(entry['field3']) # Add expenses of the same day.
                else:
                    pr_data[date_ymd] = float(entry['field2'])*float(entry['field3'])

        elif division=='months':
            pr_data = {} # Empty dictionary with all the expenses of each month.
            year = int(entry['created_at'][0:4])
            month = int(entry['created_at'][5:7])
            day = 1
            month_year = date(year, month, day)
            if month_year in pr_data:
                pr_data[month_year] += float(entry['field2'])*float(entry['field3']) # Add expenses of the same day.
            else:
                pr_data[month_year] = float(entry['field2'])*float(entry['field3'])

        data = json.dumps(pr_data)

        return data

if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8082"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8031})
    cherrypy.quickstart(ThingspeakAdaptor(),'/',conf)
