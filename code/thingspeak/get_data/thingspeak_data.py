import cherrypy
import requests
import json

class ThingspeakData(object):
    exposed=True
    def __init__(self):
        pass

    def GET(self, *uri, **params):
        message = uri[0]
        thingspeak_channel_id = params['thingspeak_channel_id']
        thingspeak_read_API = params['thingspeak_read_API']

        r = requests.get(f'https://api.thingspeak.com/channels/{thingspeak_channel_id}/feeds.json?api_key={thingspeak_read_API}')
        # LIMITATION: we take all the entries, with a maximum of 8000, which means that going shopping every day the channel would reach its limit in about 22 years.
        channel_data = r.json()
        feeds = json.dumps(channel_data['feeds'])

        return feeds


if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8081"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8030})
    cherrypy.quickstart(ThingspeakData(),'/',conf)
