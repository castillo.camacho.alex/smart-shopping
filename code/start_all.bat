@ ECHO OFF

cd .\catalog
start cmd.exe /k start.bat
ECHO Catalog

cd ..\scraping
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..\thingspeak\get_data
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..\adaptor
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..\data_processing
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..
cd ..\subscribers
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..\telegram_bot
start cmd.exe /k start.bat
start cmd.exe /k catalog_subscriber.py

cd ..\catalog
start cmd.exe /k catalog_publisher.py