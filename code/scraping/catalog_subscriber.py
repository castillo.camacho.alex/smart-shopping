from MyMQTT import *
import time
import datetime
import json
import requests

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

def update_catalog(subcatalog, body):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.put(url, data=json.dumps(body))

class CatalogSubscriber():
    def __init__(self, topic, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)
        self.topic = topic

    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)

    def stop(self):
        self.client.stop()

    def notify(self, topic, payload):
        catalog_REST = json.loads(payload)
        settings = json.load(open('settings.json'))
        settings['catalog_REST'] = catalog_REST
        json.dump(settings, open('settings.json', 'w'))

        r = requests.get('http://localhost:4040/api/tunnels/scraping')
        scraping_REST = r.json()["public_url"]
        settings = catalog_search('settings')
        settings['scraping_REST'] = scraping_REST
        update_catalog('settings', settings)


if __name__=="__main__":
    conf = json.load(open('settings.json'))
    broker = conf['broker']
    port = conf['port']
    topic = conf['baseTopic'] + '/catalog'

    catalog = CatalogSubscriber(topic, 'CatalogSubscriber', broker, port)

    catalog.start()

    while True:
        time.sleep(3)

    catalog.stop()
