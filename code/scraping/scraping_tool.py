### ENTRY INFORMATION: message: product to search; start_grid: initial search parameter. ###
import cherrypy
import requests
from lxml import html
import json

class Scraping(object):
    exposed=True
    def __init__(self):
        pass

    def GET(self, *uri, **params):
        message = uri[0]
        start_grid = params['sg']
        grid_len = params['gl']

        url = f'https://www.carrefour.it/search?q={message}&start={start_grid}'
        n = requests.get(url)
        tree = html.fromstring(n.content)

        products_list = []

        for i in range(grid_len):
            try:
                product_name = tree.xpath('//h3[@class="tile-description"]/text()')[i] + ' - ' + tree.xpath('//h2[@class="brand"]/text()')[i]
                try:
                    price = float(tree.xpath('//span[@class="value "]/text()')[i]
                            .strip('\n€ ')
                            .replace(',','.'))
                except:
                    price = float(tree.xpath('//span[@class="value discounted"]/text()')[i]
                            .strip('\n€ ')
                            .replace(',','.'))
                price_per_weight = tree.xpath('//span[@class="unit-price"]/text()')[i]
                price_per_weight = price_per_weight[:price_per_weight.find('al')+2] + ' ' + price_per_weight[price_per_weight.find('al')+3:]
                barcode = tree.xpath('//div[@class="tile-actions"]/button/@data-option-pid')[i]

                product = [barcode, product_name, price, price_per_weight]
                products_list.append(product)

            except IndexError:
                pass

        products_str = json.dumps(products_list)

        return products_str


if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8020})
    cherrypy.quickstart(Scraping(),'/',conf)
