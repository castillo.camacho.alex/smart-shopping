from MyMQTT import *
import time

class FridgeScannerPublisher():
    def __init__(self, topic, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port)
        self.topic = topic
        # Template of all the messages that I will send:
        # self.message_template = {'barcode': None}

    def start(self):
        self.client.start()

    def stop(self):
        self.client.stop()

    def publish(self, message):
        # message = self.message_template
        # message['barcode'] = barcode

        self.client.myPublish(self.topic, message)
        print('Published.')


if __name__ == '__main__':
    conf = json.load(open('settings.json'))
    broker = conf['broker']
    port = conf['port']
    topic = conf['baseTopic'] + '/add-item-home-catalog'

    fridge_scanner_manager = FridgeScannerPublisher(topic, 'FridgeScannerPublisher', broker, port)

    fridge_scanner_manager.start()

    fridge_scanner_manager.publish('3046920028721 2021 09 13 trial Àlex 3')
    fridge_scanner_manager.publish('8012666503353 2021 09 17 trial Àlex 1')

    fridge_scanner_manager.stop()
