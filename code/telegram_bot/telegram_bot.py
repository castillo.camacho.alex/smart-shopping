import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton
import json
import requests
import time
from datetime import datetime, timedelta, date
import os
from matplotlib import pyplot as plt
from dateutil.relativedelta import relativedelta

print()
print('==========================')
print(' Telegram bot initialized ')
print('==========================')


catalog_REST = json.load(open("settings.json"))['catalog_REST']

def catalog_search(subcatalog, key=None):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.get(url)
    if key==None:
        result = n.json()
    else:
        result = n.json()[key]
    return result

def update_catalog(subcatalog, body):
    url = f'{catalog_REST}/{subcatalog}'
    n = requests.put(url, data=json.dumps(body))

def adding_product_to_s_list(self, chat_ID, message, start_grid, grid_len):
    ### DATA THAT WE GET FROM THE scraping SERVICE ###
    ### We send the data message and start_grid.
    base_url = catalog_search('settings', 'scraping_REST')
    scraping_url = f'{base_url}/{message}?sg={start_grid}&gl={grid_len}'
    n = requests.get(scraping_url)
    products_list = n.json() # The list with the information of all the products.

    possible_products = []
    sent_choose_list = []

    if len(products_list) > 0:
        for prod in products_list:
            barcode = prod[0]
            product_name = prod[1]
            price = prod[2]
            price_per_weight = prod[3]

            buttons = [[InlineKeyboardButton(text=f'\U0001F197', callback_data=f'{barcode}')]]
            keyboard = InlineKeyboardMarkup(inline_keyboard=buttons)
            sent_choose_product = self.bot.sendMessage(chat_ID, text=f'{product_name}\n{price}€ ({price_per_weight})', reply_markup=keyboard)
            sent_choose_list.append(sent_choose_product)

            possible_products.append({
                'barcode': barcode,
                "product_name": product_name,
                "quantity": 1,
                "price": price
            })

    if start_grid<=0:
        buttons_arrows = [[InlineKeyboardButton(text=f'\U000027A1', callback_data=f'forward_searching')]]
    elif len(possible_products)<grid_len:
        buttons_arrows = [[InlineKeyboardButton(text=f'\U00002B05', callback_data=f'back_searching')]]
    else:
        buttons_arrows = [[InlineKeyboardButton(text=f'\U00002B05', callback_data=f'back_searching'),
                            InlineKeyboardButton(text=f'\U000027A1', callback_data=f'forward_searching')]]
    keyboard_arrows = InlineKeyboardMarkup(inline_keyboard=buttons_arrows)
    sent_arrows = self.bot.sendMessage(self.chat_ID, text=f'Previous page / Next page', reply_markup=keyboard_arrows)
    sent_choose_list.append(sent_arrows)

    return possible_products, sent_choose_list


def add_item_with_quantity(self, chat_ID, item, number, sent_message):
    item['quantity'] = number

    barcode = item['barcode']
    product_name = item['product_name']
    quantity = item['quantity']

    shopping_list = catalog_search('shopping_list', 'shopping_list')

    hh_not_in_shopping_list = True
    for hh in shopping_list:
        if hh['household']==self.household:
            hh_not_in_shopping_list = False
            hh_shopping_list = hh['products']
    if hh_not_in_shopping_list:
        shopping_list.append({"household": self.household, "products":[]})
        hh_shopping_list = []

    # Modify contents:
    for prod in hh_shopping_list:
        if prod['barcode']==barcode:
            hh_shopping_list.remove(prod)
    hh_shopping_list.append(item)

    for hh in shopping_list:
        if hh['household']==self.household:
            hh['products'] = hh_shopping_list

    update_catalog('shopping_list', shopping_list)

    sent_message_ident = (chat_ID, sent_message['message_id'])
    self.bot.deleteMessage(sent_message_ident)

    sent_finish_adding = self.bot.sendMessage(chat_ID, text=f'{quantity} unit(s) of {product_name} added to the Shopping list.')
    print(f'{quantity} unit(s) of {product_name} added to the Shopping list by {self.username} (HOUSEHOLD: {self.household}).')

    user_settings = catalog_search('user_settings', 'households')
    other_users_message = f'{self.username} added {quantity} unit(s) of {product_name} to the Shopping list.'
    for hh in user_settings:
        if hh["household_ID"]==self.household:
            users = hh['users']
    for user in users:
        chat_ID = user['chat_ID']
        if chat_ID!=self.chat_ID:
            sent_other_users = self.bot.sendMessage(chat_ID, text=other_users_message)

    self.in_choosing_quantity = False


def delete_message(self, chat_ID, message):
    sent_ident = (chat_ID, message['message_id'])
    self.bot.deleteMessage(sent_ident)


def create_thingspeak_channel(household_name):
    thingspeak_API_key = catalog_search('settings', 'thingspeak_API_key')
    requests.post('https://api.thingspeak.com/channels.json', data={'api_key': thingspeak_API_key, 'name':household_name, 'field1':'Product name', 'field2':'Price', 'field3':'Quantity'})
    r = (f'https://api.thingspeak.com/channels.json?api_key={thingspeak_API_key}')
    for channel in r.json():
        if channel['name']==household_name:
            thingspeak_channel_id = channel['id']
            for key in channel['api_keys']:
                if key['write_flag']==True:
                    thingspeak_write_API = key['api_key']
                else:
                    thingspeak_read_API = key['api_key']
    return thingspeak_write_API, thingspeak_read_API, thingspeak_channel_id



import threading

class SmartShoppingBot():
    def __init__(self, token):
        # Local token
        self.tokenBot = token

        # Catalog token
        self.bot = telepot.Bot(self.tokenBot)
        MessageLoop(self.bot, {'chat': self.on_chat_message,
                            'callback_query': self.on_callback_query}).run_as_thread()

        self.starting = 1

        self.ERROR = False

        self.in_s_list = False
        self.in_adding = False
        self.in_choosing_quantity = False
        self.in_choosing_n_items_before_shopping = False
        self.in_choosing_new_username = False
        self.in_choosing_new_household = False

    def on_chat_message(self, msg):
        content_type, chat_type, self.chat_ID = telepot.glance(msg)
        message = msg['text']

        all_chat_IDs = catalog_search('settings', 'all_chat_IDs')

        if self.chat_ID not in all_chat_IDs:
            if message == '/start' and self.starting==1:
                sent_welcome = self.bot.sendMessage(self.chat_ID, text='Welcome to the Smart Shopping Bot!')
                sent_username = self.bot.sendMessage(self.chat_ID, text='Please write your name.')
                self.starting = 2

            else:
                if self.starting==2:
                    self.username = message

                    household_buttons = [[InlineKeyboardButton(text='Joining a household.', callback_data='old_household'),
                                        InlineKeyboardButton(text='Creating a new household.', callback_data='new_household')]]
                    keyboard_household = InlineKeyboardMarkup(inline_keyboard=household_buttons)
                    sent_household = self.bot.sendMessage(self.chat_ID, text='You are...', reply_markup=keyboard_household)

                user_settings = catalog_search('user_settings.json', 'households')

                if self.starting==3:
                    for hh in user_settings:
                        if message==hh['household_ID']:
                            self.household = message
                            self.starting = 5
                    if self.starting!=5:
                        sent_household_not_found = self.bot.sendMessage(self.chat_ID, text='Household ID not found, please introduce it again.')

                elif self.starting==4:
                    for hh in user_settings:
                        household_free = True
                        if message==hh['household_ID']:
                            sent_household_already_taken = self.bot.sendMessage(self.chat_ID, text='Household ID already taken, please choose another one.')
                            household_free = False
                    if household_free:
                        self.household = message
                        print(f'Household {self.household} was created by {self.username}')
                        self.starting = 5

                if self.starting==5:
                    settings = catalog_search('settings')
                    self.user_ID = settings['user_ID_counter']+1
                    settings['user_ID_counter'] = self.user_ID
                    settings['all_chat_IDs'].append(self.chat_ID)
                    update_catalog('settings', settings)

                    specific_user_settings = {"user_ID":self.user_ID, "chat_ID":self.chat_ID, "username":self.username}

                    for hh in user_settings:
                        household_not_created = True
                        if self.household==hh['household_ID']:
                            hh['users'].append(specific_user_settings)
                            household_not_created = False

                    if household_not_created:
                        # To create a new channel in Thingspeak:
                        thingspeak_write_API, thingspeak_read_API, thingspeak_channel_id = create_thingspeak_channel(self.household)

                        # To add the new household to user_settings.json:
                        new_user_settings = catalog_search('general_household_settings_template')
                        new_user_settings['household_ID'] = self.household
                        new_user_settings['general_settings']["thingspeak_write_API"] = thingspeak_write_API
                        new_user_settings['general_settings']["thingspeak_read_API"] = thingspeak_read_API
                        new_user_settings['general_settings']["thingspeak_channel_id"] = thingspeak_channel_id
                        new_user_settings['users'].apend(specific_user_settings)
                        user_settings.append(new_user_settings)

                    update_catalog('user_settings', user_settings)

                    sent_end_initialization = self.bot.sendMessage(self.chat_ID, text='Setup completed, you can start using the Smart Shopping Bot writing /menu.')
                    print(f'User {self.username} joined the household {self.household}')
                    self.starting = 1

        else:
            user_settings = catalog_search('user_settings', 'households')
            for hh in user_settings:
                for user in hh['users']:
                    if user['chat_ID']==self.chat_ID:
                        self.household = hh['household_ID']
                        self.username = user['username']

            if message == '/menu':
                buttons = [[InlineKeyboardButton(text=f'Shopping list', callback_data=f's_list'),
                            InlineKeyboardButton(text=f'Home catalog', callback_data=f'home_catalog'),
                            InlineKeyboardButton(text=f'Add products', callback_data=f'adding'),
                            InlineKeyboardButton(text=f'Settings', callback_data=f'settings'),
                            InlineKeyboardButton(text=f'Graph', callback_data=f'graph')]]
                keyboard = InlineKeyboardMarkup(inline_keyboard=buttons)
                self.sent_menu = self.bot.sendMessage(self.chat_ID, text='Menu', reply_markup=keyboard)

                self.in_s_list = False
                self.in_adding = False
                self.in_choosing_quantity = False
                self.in_choosing_n_items_before_shopping = False
                self.in_choosing_new_username = False
                self.in_choosing_new_household = False

            elif self.in_choosing_quantity==True:
                try:
                    number = int(message)
                    add_item_with_quantity(self, self.chat_ID, self.item_to_s_list, number, self.sent_quantity_adding)
                    if self.ERROR==True:
                        error_ident = (self.chat_ID, self.sent_error['message_id'])
                        self.bot.deleteMessage(error_ident)
                        self.ERROR = False
                    error_ident = (self.chat_ID, self.sent_quantity_plus['message_id'])
                    self.bot.deleteMessage(error_ident)
                except:
                    self.ERROR = True
                    self.sent_error = self.bot.sendMessage(self.chat_ID, text="Please write a number.")

            elif self.in_adding==True:
                try:
                    for message_to_choose in self.sent_choose_list:
                        sent_choose_ident = (self.chat_ID, message_to_choose['message_id'])
                        self.bot.deleteMessage(sent_choose_ident)
                except:
                    pass
                self.product_search = message
                self.start_grid = 0
                self.grid_len = 5
                self.possible_products, self.sent_choose_list = adding_product_to_s_list(self, self.chat_ID, self.product_search, self.start_grid, self.grid_len)


            elif self.in_choosing_n_items_before_shopping==True:
                try:
                    number = int(message)
                    for hh in user_settings:
                        if hh['household_ID']==self.household:
                            hh['general_settings']['n_items_before_shopping'] = number
                    update_catalog('user_settings', user_settings)
                    if self.ERROR==True:
                        error_ident = (self.chat_ID, self.sent_error['message_id'])
                        self.bot.deleteMessage(error_ident)
                        self.ERROR = False
                    delete_message(self, self.chat_ID, self.sent_n_items_before_shopping)
                    self.in_choosing_n_items_before_shopping = False

                except:
                    self.ERROR = True
                    self.sent_error = self.bot.sendMessage(self.chat_ID, text="Please write a number.")

            elif self.in_choosing_new_username==True:
                new_username = message
                user_settings = catalog_search('user_settings', 'households')
                for hh in user_settings:
                    if hh['household_ID']==self.household:
                        for user in hh['users']:
                            if user['chat_ID']==self.chat_ID:
                                user['username'] = new_username
                                sent_new_username = self.bot.sendMessage(self.chat_ID, text=f'Username changed to {new_username}.')
                                print(f'{user}\'s username changed from {self.username} to {new_username}.')
                                self.username = new_username
                                update_catalog('user_settings', user_settings)
                                if self.ERROR==True:
                                    error_ident = (self.chat_ID, self.sent_error['message_id'])
                                    self.bot.deleteMessage(error_ident)
                                    self.ERROR = False
                                delete_message(self, self.chat_ID, self.sent_change_user_information)
                                self.in_choosing_new_username = False

            elif self.in_choosing_new_household==True:
                new_household = message
                user_not_changed_households = True
                for hh in user_settings:
                    if hh['household_ID']==self.household:
                        for user in hh['users']:
                            if user['chat_ID']==self.chat_ID:
                                for new_hh in user_settings:
                                    if new_hh['household_ID']==new_household:
                                        new_hh["users"].append(user)
                                        hh["users"].remove(user)
                                        sent_new_household = self.bot.sendMessage(self.chat_ID, text=f'Moved to household {new_household}.')
                                        print(f'{user} changed from household {self.household} to household {new_household}.')
                                        user_not_changed_households = False
                                if user_not_changed_households:
                                    thingspeak_write_API, thingspeak_read_API, thingspeak_channel_id = create_thingspeak_channel(new_household)
                                    household_template = catalog_search('general_household_settings_template')
                                    household_template["household_ID"] = new_household
                                    household_template["thingspeak_write_API"] = thingspeak_write_API
                                    household_template["thingspeak_read_API"] = thingspeak_read_API
                                    household_template["thingspeak_channel_id"] = thingspeak_channel_id
                                    household_template["users"].append(user)
                                    user_settings.append(household_template)
                                    sent_new_household = self.bot.sendMessage(self.chat_ID, text=f'Created and moved to household {new_household}.')
                                    print(f'{user} changed from household {self.household} to (new) household {new_household}.')
                self.household = new_household
                update_catalog('user_settings', user_settings)
                if self.ERROR==True:
                    error_ident = (self.chat_ID, self.sent_error['message_id'])
                    self.bot.deleteMessage(error_ident)
                    self.ERROR = False
                delete_message(self, self.chat_ID, self.sent_change_user_information)
                self.in_choosing_new_household = False

            else:
                self.ERROR = True
                self.sent_error = self.bot.sendMessage(self.chat_ID, text="Command not supported")


    def on_callback_query(self, msg):
        query_ID, self.chat_ID, query_data = telepot.glance(msg, flavor='callback_query')

        if query_data == 'old_household':
            sent_household = self.bot.sendMessage(self.chat_ID, text='Please write the household ID.')
            self.starting = 3

        if query_data == 'new_household':
            sent_household = self.bot.sendMessage(self.chat_ID, text='Write an ID for your household.')
            self.starting = 4

        if query_data == 's_list':
            ## SHOW SHOPPING LIST
            sent_start = self.bot.sendMessage(self.chat_ID, text='Shopping started.')

            shopping_list = catalog_search('shopping_list', 'shopping_list')

            hh_not_in_shopping_list = True
            for hh in shopping_list:
                if hh['household']==self.household:
                    hh_not_in_shopping_list = False

            if hh_not_in_shopping_list:
                shopping_list.append({"household": self.household, "products":[]})

            for hh in shopping_list:
                if hh['household']==self.household:
                    self.s_list = hh['products']

            self.buttons = []
            for product in self.s_list:
                barcode = product['barcode']
                product_name = product['product_name']
                quantity = product['quantity']
                self.buttons.append([InlineKeyboardButton(text=f'({quantity}) {product_name}', callback_data=f'{barcode}')])

            self.buttons.append([InlineKeyboardButton(text='\U0001F6CD FINISH SHOPPING \U0001F6CD', callback_data=f'finish')])

            keyboard = InlineKeyboardMarkup(inline_keyboard=self.buttons)
            self.sent_s_list = self.bot.sendMessage(self.chat_ID, text='Shopping list', reply_markup=keyboard)

            self.in_s_list = True
            self.removing_products = []

        if self.in_s_list==True:
            for product in self.s_list:
                barcode = product['barcode']
                product_name = product['product_name']

                if query_data == barcode:
                    self.removing_products.append(product)

                    # query_ID, self.chat_ID, query_data = telepot.glance(msg, flavor='callback_query')

                    for i in range(len(self.buttons)):
                        if self.buttons[i][0][2]==barcode:
                            self.buttons[i][0] = InlineKeyboardButton(text=f'\U00002705 {product_name}', callback_data=f'{barcode}_check')

                    keyboard2 = InlineKeyboardMarkup(inline_keyboard=self.buttons)
                    # sent = self.bot.sendMessage(self.chat_ID, text='Shopping list', reply_markup=keyboard)
                    self.bot.editMessageReplyMarkup(telepot.message_identifier(self.sent_s_list), keyboard2)

                elif query_data == f'{barcode}_check':
                    self.removing_products.remove(product)

                    # query_ID, self.chat_ID, query_data = telepot.glance(msg, flavor='callback_query')

                    for i in range(len(self.buttons)):
                        if self.buttons[i][0][2]==f'{barcode}_check':
                            self.buttons[i][0] = InlineKeyboardButton(text=f'{product_name}', callback_data=f'{barcode}')

                    keyboard2 = InlineKeyboardMarkup(inline_keyboard=self.buttons)
                    # sent = self.bot.sendMessage(self.chat_ID, text='Shopping list', reply_markup=keyboard)
                    self.bot.editMessageReplyMarkup(telepot.message_identifier(self.sent_s_list), keyboard2)

                elif query_data == 'finish':
                    ## MODIFY SHOPPING LIST
                    sent_finish = self.bot.sendMessage(self.chat_ID, text='Shopping list saved.')

                    menu_ident = (self.chat_ID, self.sent_menu['message_id'])
                    self.bot.deleteMessage(menu_ident)

                    s_list_ident = (self.chat_ID, self.sent_s_list['message_id'])
                    self.bot.deleteMessage(s_list_ident)

                    if self.ERROR==True:
                        error_ident = (self.chat_ID, self.sent_error['message_id'])
                        self.bot.deleteMessage(error_ident)
                        self.ERROR = False

                    # To remove the products of the shopping list:
                    shopping_list = catalog_search('shopping_list', 'shopping_list')

                    for hh in shopping_list:
                        if hh['household']==self.household:
                            hh_shopping_list = hh['products']

                    for product in self.removing_products:
                        # Modify contents:
                        hh_shopping_list.remove(product)

                        print(f'{product["product_name"]} removed from the Shopping list by {self.username} (HOUSEHOLD: {self.household}).')

                        user_settings = catalog_search('user_settings', 'households')
                        other_users_message = f'{self.username} removed {product["product_name"]} from the Shopping list.'
                        for hh in user_settings:
                            if hh["household_ID"]==self.household:
                                users = hh['users']
                        for user in users:
                            chat_ID = user['chat_ID']
                            if chat_ID!=self.chat_ID:
                                sent_other_users = self.bot.sendMessage(chat_ID, text=other_users_message)

                    for hh in shopping_list:
                        if hh == self.household:
                            hh['products'] = hh_shopping_list

                    update_catalog('shopping_list', shopping_list)

                    break

        if query_data == 'back_searching':
            for message in self.sent_choose_list:
                sent_choose_ident = (self.chat_ID, message['message_id'])
                self.bot.deleteMessage(sent_choose_ident)
            if self.start_grid > 0:
                self.start_grid -= self.grid_len
            self.possible_products, self.sent_choose_list = adding_product_to_s_list(self, self.chat_ID, self.product_search, self.start_grid, self.grid_len)

        elif query_data == 'forward_searching':
            for message in self.sent_choose_list:
                sent_choose_ident = (self.chat_ID, message['message_id'])
                self.bot.deleteMessage(sent_choose_ident)
            self.start_grid += self.grid_len
            self.possible_products, self.sent_choose_list = adding_product_to_s_list(self, self.chat_ID, self.product_search, self.start_grid, self.grid_len)

        elif query_data == 'adding':
            return_button = [[InlineKeyboardButton(text='Return', callback_data=f'return_from_adding')]]
            keyboard = InlineKeyboardMarkup(inline_keyboard=return_button)
            self.sent_adding = self.bot.sendMessage(self.chat_ID, text='Please write the products to add:', reply_markup=keyboard)

            self.in_adding = True

        elif query_data == 'return_from_adding':
            self.in_adding = False

            adding_ident = (self.chat_ID, self.sent_adding['message_id'])
            self.bot.deleteMessage(adding_ident)

        elif query_data == 'quantity=1':
            add_item_with_quantity(self, self.chat_ID, self.item_to_s_list, 1, self.sent_quantity_adding)

        elif query_data == 'quantity=2':
            add_item_with_quantity(self, self.chat_ID, self.item_to_s_list, 2, self.sent_quantity_adding)

        elif query_data == 'quantity=3':
            add_item_with_quantity(self, self.chat_ID, self.item_to_s_list, 3, self.sent_quantity_adding)

        elif query_data == 'quantity=+':
            self.sent_quantity_plus = self.bot.sendMessage(self.chat_ID, text='Quantity:')
            self.in_choosing_quantity = True

        elif self.in_adding==True:
            try:
                for message in self.sent_choose_list:
                    sent_choose_ident = (self.chat_ID, message['message_id'])
                    self.bot.deleteMessage(sent_choose_ident)
            except:
                pass

            barcode = query_data
            for prod in self.possible_products:
                if prod['barcode']==barcode:
                    self.item_to_s_list = prod
            print(f'\nitem to s list: {self.item_to_s_list}')

            quantity_numbers_buttons = [[InlineKeyboardButton(text='\U00000031', callback_data=f'quantity=1'),
                                        InlineKeyboardButton(text='\U00000032', callback_data=f'quantity=2'),
                                        InlineKeyboardButton(text='\U00000033', callback_data=f'quantity=3'),
                                        InlineKeyboardButton(text='\U00002795', callback_data=f'quantity=+')]]
            keyboard_quantity_numbers = InlineKeyboardMarkup(inline_keyboard=quantity_numbers_buttons)
            self.sent_quantity_adding = self.bot.sendMessage(self.chat_ID, text='Choose quantity:', reply_markup=keyboard_quantity_numbers)

        if query_data == 'home_catalog':
            ## SHOW HOME CATALOG
            home_catalog = catalog_search('home_catalog', 'home_catalog')

            hh_not_in_home_catalog = True
            for hh in home_catalog:
                if hh['household']==self.household:
                    hh_not_in_home_catalog = False

            if hh_not_in_home_catalog:
                home_catalog.append({"household": self.household, "products":[]})

            for hh in home_catalog:
                if hh['household']==self.household:
                    self.h_catalog = hh['products']

            h_catalog_text = 'Home catalog:'
            for product in self.h_catalog:
                barcode = product['barcode']
                product_name = product['product_name']
                quantity = product['quantity']
                h_catalog_text += f'\n  - {quantity} units of {product_name}'

            self.buttons = [[InlineKeyboardButton(text='RETURN', callback_data=f'return_from_h_catalog')]]

            keyboard = InlineKeyboardMarkup(inline_keyboard=self.buttons)
            self.sent_h_catalog = self.bot.sendMessage(self.chat_ID, text=h_catalog_text, reply_markup=keyboard)

        if query_data == 'return_from_h_catalog':
            menu_ident = (self.chat_ID, self.sent_menu['message_id'])
            self.bot.deleteMessage(menu_ident)

            h_catalog_ident = (self.chat_ID, self.sent_h_catalog['message_id'])
            self.bot.deleteMessage(h_catalog_ident)

            if self.ERROR==True:
                error_ident = (self.chat_ID, self.sent_error['message_id'])
                self.bot.deleteMessage(error_ident)
                self.ERROR = False

        if query_data == 'settings':
            settings_buttons = [[InlineKeyboardButton(text='Automatic adding to the Shopping list', callback_data=f'n_items_before_shopping')],
                                [InlineKeyboardButton(text='User information', callback_data=f'user_information')]]
            keyboard_settings = InlineKeyboardMarkup(inline_keyboard=settings_buttons)
            self.sent_settings = self.bot.sendMessage(self.chat_ID, text='Settings:', reply_markup=keyboard_settings)

        if query_data == 'n_items_before_shopping':
            delete_message(self, self.chat_ID, self.sent_settings)
            user_settings = catalog_search('user_settings', 'households')
            for hh in user_settings:
                if hh["household_ID"]==self.household:
                    n_items_before_shopping = hh['general_settings']['n_items_before_shopping']
            n_items_before_shopping_buttons = [[InlineKeyboardButton(text='Return', callback_data=f'n_items_before_shopping_return')]]
            keyboard_n_items_before_shopping = InlineKeyboardMarkup(inline_keyboard=n_items_before_shopping_buttons)
            self.sent_n_items_before_shopping = self.bot.sendMessage(self.chat_ID, text=f'The number of items left in storage for adding the product to the shopping list is set to: {n_items_before_shopping}.\nPlease write another number or press Return.', reply_markup=keyboard_n_items_before_shopping)

            self.in_choosing_n_items_before_shopping = True

        if query_data == 'n_items_before_shopping_return':
            delete_message(self, self.chat_ID, self.sent_n_items_before_shopping)
            self.in_choosing_n_items_before_shopping = False

        if query_data == 'user_information':
            user_information_buttons = [[InlineKeyboardButton(text='Username', callback_data=f'change_username')],
                                        [InlineKeyboardButton(text='Household ID', callback_data=f'change_household')]]
            keyboard_user_information = InlineKeyboardMarkup(inline_keyboard=user_information_buttons)
            self.sent_user_information = self.bot.sendMessage(self.chat_ID, text='User information:', reply_markup=keyboard_user_information)

        if query_data == 'change_username':
            delete_message(self, self.chat_ID, self.sent_user_information)
            change_username_buttons = [[InlineKeyboardButton(text='Return', callback_data=f'user_information_return')]]
            keyboard_change_username = InlineKeyboardMarkup(inline_keyboard=change_username_buttons)
            self.sent_change_user_information = self.bot.sendMessage(self.chat_ID, text=f'Your username is set to: {self.username}.\nPlease write another one or press Return.', reply_markup=keyboard_change_username)
            self.in_choosing_new_username = True

        if query_data == 'change_household':
            delete_message(self, self.chat_ID, self.sent_user_information)
            change_household_buttons = [[InlineKeyboardButton(text='Return', callback_data=f'user_information_return')]]
            keyboard_change_household = InlineKeyboardMarkup(inline_keyboard=change_household_buttons)
            self.sent_change_user_information = self.bot.sendMessage(self.chat_ID, text=f'Your household ID is set to: {self.household}.\nPlease write another ID or press Return.', reply_markup=keyboard_change_household)
            self.in_choosing_new_household = True

        if query_data == 'user_information_return':
            delete_message(self, self.chat_ID, self.sent_change_user_information)
            self.in_choosing_new_username = False
            self.in_choosing_new_household = False

        if query_data == 'graph':
            graphs_buttons = [[InlineKeyboardButton(text='Monthly expenses (1 year)', callback_data=f'graph1')],
                                        [InlineKeyboardButton(text='Daily expenses (1 month)', callback_data=f'graph2')],
                                        [InlineKeyboardButton(text='Daily expenses (1 week)', callback_data=f'graph3')]]
            keyboard_graphs = InlineKeyboardMarkup(inline_keyboard=graphs_buttons)
            self.sent_graphs = self.bot.sendMessage(self.chat_ID, text=f'Options to plot:', reply_markup=keyboard_graphs)


        if query_data=='graph1' or query_data=='graph2' or query_data=='graph3':
            user_settings = catalog_search('user_settings', 'households')
            for hh in user_settings:
                if hh["household_ID"]==self.household:
                    thingspeak_write_API = hh['general_settings']['thingspeak_write_API']
                    thingspeak_read_API = hh['general_settings']['thingspeak_read_API']
                    thingspeak_channel_id = hh['general_settings']['thingspeak_channel_id']

            # # r = requests.get(f'https://api.thingspeak.com/channels/1425656/feeds.json?api_key=CSJPGH6BJLWIJCJG')
            # r = requests.get(f'https://api.thingspeak.com/channels/{thingspeak_channel_id}/feeds.json?api_key={thingspeak_read_API}')
            # # LIMITATION: we take all the entries, with a maximum of 8000, which means that going shopping every day the channel would reach its limit in about 22 years.
            # channel_data = r.json()
            # feeds = channel_data['feeds']

            # feeds = [{'created_at': '2020-10-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '73', 'field3': '1'}, {'created_at': '2020-11-24T16:09:09Z', 'entry_id': 2, 'field1': 'milk', 'field2': '67.4', 'field3': '1'}, {'created_at': '2020-12-02T15:04:33Z', 'entry_id': 3, 'field1': 'Cheese', 'field2': '95.7', 'field3': '1'},{'created_at': '2021-01-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '89.5', 'field3': '1'},{'created_at': '2021-02-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '61.9', 'field3': '1'},{'created_at': '2021-03-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '52.6', 'field3': '1'},{'created_at': '2021-04-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '26.9', 'field3': '2'},{'created_at': '2021-05-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '71', 'field3': '1'},{'created_at': '2021-06-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '69.3', 'field3': '1'},{'created_at': '2021-07-24T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '62.2', 'field3': '1'},{'created_at': '2021-08-02T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '25', 'field3': '1'},{'created_at': '2021-08-10T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '14.50', 'field3': '1'},{'created_at': '2021-08-15T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '13.7', 'field3': '1'},{'created_at': '2021-08-17T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '20.2', 'field3': '1'},{'created_at': '2021-08-23T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '6.9', 'field3': '1'},{'created_at': '2021-08-28T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '24.7', 'field3': '1'},{'created_at': '2021-08-30T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '7.7', 'field3': '1'},{'created_at': '2021-09-02T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '16.1', 'field3': '1'},{'created_at': '2021-09-05T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '9.9', 'field3': '1'},{'created_at': '2021-09-09T16:08:48Z', 'entry_id': 1, 'field1': 'cheese', 'field2': '11.9', 'field3': '1'}]

            option = query_data[-1]
            print(f'\nPLOT OPTION {option}')

            base_url = catalog_search('settings', 'thingspeak_processing_REST')
            figure = requests.get(f'{base_url}/{option}?thingspeak_channel_id={thingspeak_channel_id}&thingspeak_read_API={thingspeak_read_API}')
            self.photo_graphs = self.bot.sendPhoto(self.chat_ID, figure)

            # file = open("sample_image.png", "wb")
            # file.write(response.content)
            # file.close()
            # time_period, expenses, title, x_axis = data_processing(self.chat_ID, option, feeds)

            # Plotting:
            # IMPORTANT: THINGSPEAK PLOT.
            # print(f'https://api.thingspeak.com/channels/{processing_channel_id}/charts/1?api_key={processing_read_api_key}&type=column&title=Monthly expenses in a year&xaxis=Months&yaxis=Expenses')

            ## Plotting in Python:
            # plt.figure()
            # print('Plotting...')
            # plt.bar(time_period, expenses, width=0.5)
            # print('Plot done. Saving...')
            # plt.title(title)
            # plt.xlabel(x_axis)
            # plt.xticks(rotation=45, ha='right')
            # plt.ylabel('Expenses')
            # filename = f'expenses_option{option}_{self.household}_{self.username}_{datetime.now().timestamp()}.png'
            # print(f'filename: {filename}')
            # plt.savefig(filename)
            # print('Plot saved. Sending...')
            # self.photo_graphs = self.bot.sendPhoto(self.chat_ID, open(filename, 'rb'))
            # os.remove(filename)
            # print('Plot sent.')


# class ExpiryDateCheckThread(threading.Thread):
#     def __init__(self, bot, chat_ID):
#         threading.Thread.__init__(self)
#         #Setup thread
#         self.bot = bot
#         self.chat_ID = chat_ID
#
#     def run(self):
#         while True:
#             sent_initial_expiry_date = self.bot.sendMessage(self.chat_ID, text='Checking expiry dates.')
#             time.sleep(5)
#
# class SmartShoppingBot():
#     def __init__(self, token):
#         # Local token
#         self.tokenBot = token
#
#         # Catalog token
#         self.bot = telepot.Bot(self.tokenBot)
#
#         thread_main = MainBotThread(self.bot)
#         thread_main.start()
#
#         MessageLoop(self.bot, {'chat': thread_main.on_chat_message,
#                             'callback_query': thread_main.on_callback_query}).run_as_thread()
#
#         # thread_check_ed = ExpiryDateCheckThread(self.bot, chat_ID)
#         # thread_check_ed.start()


def check_expiry_date(bot):
    print('Checking expiry dates...')

    home_catalog = catalog_search('home_catalog', 'home_catalog')
    user_settings = catalog_search('user_settings', 'households')

    current_date = date.today()

    for hh in home_catalog:
        for product in hh['products']:
            expiry_date_list = product['expiry_date']

            if expiry_date_list!=[]:
                expiry_date = date(expiry_date_list[2], expiry_date_list[1], expiry_date_list[0])
                time_distance = (expiry_date - current_date).days
                product_name = home_catalog[household][product]['product_name']

                if home_catalog[household][product]['quantity']!=0 and time_distance<=2:
                    if time_distance<-1:
                        expiry_message = f'{product_name} expired {-time_distance} days ago.'
                    elif time_distance==-1:
                        expiry_message = f'{product_name} expired yesterday.'
                    elif time_distance==0:
                        expiry_message = f'{product_name} expires today.'
                    elif time_distance==1:
                        expiry_message = f'{product_name} expires tomorrow.'
                    elif time_distance==2:
                        expiry_message = f'{product_name} expires in 2 days.'

                    for hh in user_settings:
                        if hh["household_ID"]==household:
                            users = hh["users"]
                    for user in users:
                        chat_ID = user['chat_ID']
                        sent_expiry = bot.sendMessage(chat_ID, text=expiry_message)

import schedule

if __name__ == "__main__":
    token = catalog_search('settings', 'telegram_token')

    # Smart shopping bot
    ssbot = SmartShoppingBot(token)

    schedule.every(1).day.at("11:48").do(check_expiry_date, ssbot.bot)

    while True:
        schedule.run_pending()
