# Problems:
  - Monolithical application
    - Good practices
  - Catalog is not an actual catalog
  - We communicate directly with files, and several actors at the same time, so there's a problem (inconsistency).
    - Only one actor can communicate with the jsons.

# Info:
  - The catalog is a functionality with the database and the data manager, that sends the information via REST or MQTT.
  - Netflix, amazon, linksmart have specific catalogs, with their own choices.
  - Threading has nothing to do with microservices, it doesn't divide the functionalities.

# Things to do:
  - Creating Thingspeak Adaptor, so we only have to change the adaptor if we change the source of data (instead of thingspeak another one).
    - In the end thingspeak is three parts: the one with the data (REST), the one that organizes the data (the Adaptor) and the one that processes the data and sends it.
  - Also separate the Price-scraping tool, so that it can be adapted to take data from other databases.
  - All jsons change specific keys for generic (en lloc d'utilitzar el nom com a key, el posem com a característica, de forma que haurem de fer un if es troba això, aleshores utilitzes aquestes dades).
  - Connection with all the functions not as libraries but with REST (or MQTT).
  - Change the telegram and thingspeak api keys (the basic ones), to include them in settings, instead of written directly in code.
    - Each microservice will have its own settings file, that will be the only file accessed directly, it has to be local to each microservice.
  - In the end, we divide the microservices between us, so we execute them in different computers and it's actually a distributed approach.
    - We also try to divide them before in order to find bugs and run them in different computers. That way we can find also inconsistencies and stuff like that.
  - We could use threads also to divide other services.
